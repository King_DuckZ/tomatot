import deimos.ncurses.ncurses;
import std.conv: to;
import std.string: toStringz;
import textpiece;

class Surf {
	this() {
		m_text = new TextPiece();
		m_withColors = has_colors();
	}

	void Draw() {
		if (m_withColors) {
			start_color();
			init_pair(1, COLOR_RED, COLOR_BLACK);
			attron(COLOR_PAIR(1));
		}

		print_in_middle(stdscr, m_text, LINES / 2, 0, 0, );
		refresh();

		if (m_withColors) {
			attroff(COLOR_PAIR(1));
		}
	}

	@property bool withColors() { return m_withColors; }
	@property TextPiece text (TextPiece parText) { return m_text = parText; }
	@property TextPiece text () { return m_text; }

private:
	TextPiece m_text;
	const bool m_withColors;

	void print_in_middle(WINDOW *win, TextPiece str, int starty = LINES/2, int startx=0, int width=0) {
		int length, x, yfrom;

		if(win == null)
			win = stdscr;
		getyx(win, yfrom, x);
		if(startx != 0)
			x = startx;
		if(starty != 0)
			yfrom = starty;
		if(width == 0)
			width = 80;

		//int already takes the floor, we can change temp to int.
		float temp = (width - str.textWidth)/ 2;
		x = startx + temp.to!int;

		for (int y = yfrom; y < yfrom + str.textHeight; ++y) {
			mvwprintw(win, y, x, "%s", toStringz(str.getLine(y - yfrom)));
		}
	}
};
