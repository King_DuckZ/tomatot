class TextPiece {
	this() {
		m_lines = [];
	}

	void AddLine (string parLine) {
		if (cast(int)(parLine.length) > m_maxWidth)
			m_maxWidth = cast(int)(parLine.length);

		m_lines ~= parLine;
	}

	string getLine (int parIndex) {
		return m_lines[parIndex];
	}

	@property int textHeight() { return cast(int)(m_lines.length); }
	@property int textWidth() { return m_maxWidth; }

private:
	string[] m_lines;
	int m_maxWidth;
};
