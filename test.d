import deimos.ncurses.ncurses;
import surf;
import std.stdio;
import textpiece;

void main() {
	initscr();
	Surf surf = new Surf();
	surf.text.AddLine("Hello from the middle of the screen");
	surf.text.AddLine("Adding a second line...");
	surf.text.AddLine("...and even a third!");

	scope(exit) endwin();

	surf.Draw();

	endwin();
}
