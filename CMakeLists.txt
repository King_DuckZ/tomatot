cmake_minimum_required(VERSION 2.8)
project(tomatot D)

find_package(Curses REQUIRED)
include_directories(SYSTEM
	${CMAKE_SOURCE_DIR}/ncurses
)
include_directories(
	.
)

add_executable(${PROJECT_NAME}
	surf.d
	textpiece.d
	test.d
)

target_link_libraries(${PROJECT_NAME}
	curses
)
